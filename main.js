if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

var Bind = function (regexpA, replaceValue) {
    this.regexpA = regexpA;
    this.replaceValue = replaceValue;


};

Bind.add = function (regexpA, replaceValue) {
    var _binds = JSON.parse(localStorage.binds);
    var id = Bind.updateID;
    if (id == -1) {
        id = _binds.length;
    } else {
        Bind.updateID = -1;
    }
    _binds[id] = new Bind(regexpA, replaceValue);
    localStorage.binds = JSON.stringify(_binds);
    return id;
};

Bind.deleteEntry = function (id) {
    var _binds = JSON.parse(localStorage.binds);
    if (_binds[id]) {
        _binds[id] = null;
        localStorage.binds = JSON.stringify(_binds);
    }
};


Bind.updateID = -1;

Bind.listAll = function () {

    return JSON.parse(localStorage.binds);
};

Bind.deleteFromTable = function (e) {
    Bind.deleteEntry(e.target.parentNode.getAttribute("data-tw-loca-id"));
    e.target.parentNode.parentNode.parentNode.removeChild(e.target.parentNode.parentNode);

};

Bind.updateFromTable  =function(e){
    Bind.updateID = parseInt(e.target.parentNode.getAttribute("data-tw-loca-id"));
    var regexpField = document.querySelector("#TW_Loca_RegExp");
    var replaceValue = document.querySelector("#TW_Loca_Replace");





    Bind.deleteFromTable(e);
};


var Loca = function () {
    this.entry = null;
    var _namekey = document.querySelector("#constantlistTbl table td:nth-child(2)").innerText.substr(10);
    var _input = document.querySelectorAll("input[name^=data_translated_],textarea[name^=data_translated_]");
    var _save = document.querySelector("input[type=submit][name=store]");
    var _binds;



    if (!localStorage.binds) localStorage.binds = "[]";

    _binds = Bind.listAll();

    this.isReferencekey = function () {
        return _namekey.substr(0, 7) == "z_trans";
    };

    this.getNamekey = function () {
        return _namekey;
    };

    var translate = function (text, bind) {
        var regexp = new RegExp(bind.regexpA[0], bind.regexpA[1]);

        if (bind.replaceValue[1] && bind.replaceValue.length == 2) {
            bind.replaceValue = eval("(" + bind.replaceValue[0] + ")");
        } else if(bind.replaceValue.length == 2){
            bind.replaceValue = bind.replaceValue[0];
        }

        return text.replace(regexp, bind.replaceValue);
    };

    this.translation = function (text,entry) {
        _binds = Bind.listAll();
        var i;
        for (i=_binds.length;i>=0;i--) {
            if (_binds.hasOwnProperty(i) && _binds[i] != null) {

                text = translate(text, _binds[i]);
            }
        }
        _input[entry].value = text;
        _save.focus();
    };
};

Loca.reference = function () {
    var keys = document.querySelectorAll("#referencetrans");
    var finishedKeys = document.querySelectorAll("#referencetrans.keyTranslationStatusreview");
    var infoBox = "<span class='name'>" + l.getNamekey() + "</span><br/>\n" +
        "Keys: (" + finishedKeys.length + "/" + keys.length + ")<br/>\n";
    var container = document.createElement("div");

    container.innerHTML = infoBox;
    container.setAttribute("class", "infobox");

    document.body.appendChild(container);

};

Loca.prompt = function(text,value){
    var val = prompt(text,value);
    if(val === null) val = Loca.prompt(text,value);
    return val;
};

Loca.trans = function () {
    useValue = document.querySelectorAll(".useValueButton");
    if (useValue.length == 0) {
        setTimeout(Loca.trans, 100);

    } else {

        for(Loca.entry in useValue ) {
            if(!Loca.entry.match(/^\d+$/)) continue;

            var translatorButton = document.createElement("button");
            translatorButton.appendChild(document.createTextNode("-[]->"));
            translatorButton.value = "--->";
            translatorButton.dataset.entry = Loca.entry;
            translatorButton.addEventListener("click", function () {
                l.translation(document.querySelectorAll("input.source-translation,textarea.source-translation")[this.dataset.entry].value.trim(),this.dataset.entry);
            });



            useValue[Loca.entry].parentNode.insertBefore(translatorButton, useValue[Loca.entry]);
            useValue[Loca.entry].style.display="none";
            translatorButton.focus();
            var regexpAddbox = "<div id='TW_Loca_hide'>[-]</div><label class='name'>Regular Expression:<br/><input id='TW_Loca_RegExp' type='text' ></label><br/>\n" +
                "<span class='name'>Regexp Options:</span><br/>" +
                "<label title='Ignore Case'>[<input id='TW_Loca_RegExp_opt_i' value='i' checked='checked' type='checkbox'> i] </label>" +
                "<label title='Global Match'>[<input id='TW_Loca_RegExp_opt_g' value='g' checked='checked' type='checkbox'> g] </label>" +
                "<label title='Multi-line (each line)'>[<input id='TW_Loca_RegExp_opt_m' value='m' type='checkbox'> m] </label>" +
                "<label title='Sticky (Experimental)'>[<input id='TW_Loca_RegExp_opt_y' value='y' type='checkbox'> y] </label><br/>\n" +
                "<span class='name'><label for='TW_Loca_Replace'>Replace with:</label> </span><label>[<input id='TW_Loca_Replace_opt_func' type='checkbox'> As function]</label><br/><textarea id='TW_Loca_Replace'></textarea><br/>" +
                "<input type='button' value='Add' id='TW_Loca_Add'><input type='button' value='Clear' id='TW_Loca_CF'><br/>\n"+
                "<input type='button' value='Cleanup' id='TW_Loca_Cleanup'><input type='button' value='Origin Translate' id='TW_Loca_Origin'>";
        }

        var container = document.createElement("div");
        container.innerHTML = regexpAddbox;
        container.setAttribute("class", "infobox");

        container.appendChild(document.createElement("hr"));

        var table = document.createElement("table");
        var tr = document.createElement("tr");

        tr.innerHTML = "<th>ID</th><th>Regexp</th><th>Replace Value</th><th>E/D</th>";
        table.appendChild(tr);

        var binds = Bind.listAll();

        for (var id = 0; id < binds.length; id++) {
            tr = document.createElement("tr");
            if (binds[id] == null) continue;
            tr.innerHTML = "<td>" + id + "</td><td>/" + htmlEntities(binds[id].regexpA[0]) + "/" + htmlEntities(binds[id].regexpA[1]) + "</td><td" + (binds[id].replaceValue[1] ? " class='function'" : "") + ">" + htmlEntities(binds[id].replaceValue[0]) + "</td><td data-tw-loca-id='" + id + "'><span class='TW_Loca_edit'>[E]</span> <span class='TW_Loca_delete'>[D]</span></td>";

            tr.querySelector("td[data-tw-loca-id='" + id + "'] .TW_Loca_delete").addEventListener("click", Bind.deleteFromTable);

            table.appendChild(tr);
        }
        container.appendChild(table);


        document.body.appendChild(container);

        var addButton = document.querySelector("#TW_Loca_Add");
        var clearButton = document.querySelector("#TW_Loca_CF");
        var cleanButton = document.querySelector("#TW_Loca_Cleanup");
        var originButton = document.querySelector("#TW_Loca_Origin");

        var hide_link = document.querySelector("#TW_Loca_hide");
        var cleanBinds = function(){
            var bonds = JSON.parse(localStorage.getItem("binds"));
            var new_binds = [];
            for(var i=0;i<bonds.length;i++){
                if(bonds[i] == null) continue;
                new_binds.push(bonds[i]);
            }
            localStorage.setItem("binds",JSON.stringify(new_binds));
            location.reload(true);
        };

        var clearForm = function () {
            document.querySelector("#TW_Loca_RegExp").value = "";
            document.querySelector("#TW_Loca_Replace").value = "";
        };
        var addBind = function () {
            var regexpField = document.querySelector("#TW_Loca_RegExp");
            var regexpOptionsFields = document.querySelectorAll(".infobox input[type='checkbox'][id^='TW_Loca_RegExp_opt_']");
            var replaceValueField = document.querySelector("#TW_Loca_Replace");
            var replaceValueFunction = document.querySelector("#TW_Loca_Replace_opt_func").checked;
            var regexp = regexpField.value;
            var replaceValue = replaceValueField.value.trim();
            var regexpOptions = "";

            var field;
            for (var i = 0; i < regexpOptionsFields.length; i++) {

                field = regexpOptionsFields.item(i);

                if (field.checked) {
                    regexpOptions += field.value;
                }
            }
            var error = [];
            try {
                new RegExp(regexp);

            } catch (e) {
                error.push(e.message);
            }
            if (replaceValueFunction) {
                try {
                    eval("(" + replaceValue + ")");
                } catch (e) {
                    error.push(e.message);
                }
            }
            if (error.length > 0) {
                alert(error.join("\n"));
            } else {

                var id = Bind.add([regexp, regexpOptions], [replaceValue, replaceValueFunction]);
                tr = document.createElement("tr");
                tr.innerHTML = "<td>" + id + "</td><td>/" + htmlEntities(regexp) + "/" + htmlEntities(regexpOptions) + "</td><td" + (replaceValueFunction ? " class='function'" : "") + ">" + htmlEntities(replaceValue) + "</td><td data-tw-loca-id='" + id + "'><span class='TW_Loca_edit'>[E]</span> <span class='TW_Loca_delete'>[D]</span></td>";
                table.appendChild(tr);

                document.querySelector("td[data-tw-loca-id='" + id + "'] .TW_Loca_delete").addEventListener("click", Bind.updateFromTable);
                document.querySelector("td[data-tw-loca-id='" + id + "'] .TW_Loca_edit").addEventListener("click", Bind.deleteFromTable);
                clearForm();
            }
        };



        hide_link.addEventListener("click", function () {
            if (container.hasAttribute("data-TW-show")) {
                container.setAttribute("class", "infobox");
                container.removeAttribute("data-TW-show");
            } else {
                container.setAttribute("class", "infobox hidden_infobox");
                container.setAttribute("data-TW-show", "true");
            }


        });
        clearButton.addEventListener("click", clearForm);
        addButton.addEventListener("click", addBind);
        cleanButton.addEventListener("click",cleanBinds);
        originButton.addEventListener("click",function(e){
            var OT = eval(localStorage.getItem("Origin-translate"));

            e.target.value = (OT) ? "Origin Translate Off" : "Origin Translate On";

            localStorage.setItem("Origin-translate",!OT);
        });


    }
};

var l = new Loca();
if (!l.isReferencekey()) {
    var useValue;
    setTimeout(Loca.trans, 100);
} else Loca.reference();


function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}